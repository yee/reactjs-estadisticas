# [Statistics Football](http://antonioyee.mx/estadisticas)
Statistics Football League in ReactJS

### Installation
1. `npm install`
3. `gulp`
2. `npm start`

### License
MIT

### Developer
*Antonio Yee*

- [yee.antonio@gmail.com](mailto:yee.antonio@gmail.com)
- <http://antonioyee.mx>
- <https://twitter.com/antonioyee>
