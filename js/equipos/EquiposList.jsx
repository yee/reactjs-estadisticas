import React from 'react';
import LogoImg from '../components/LogoImg';
import PanelHeading from '../components/PanelHeading';

export default class EquiposList extends React.Component {

    render(){
        return(
            <div className="col-sm-6 col-md-6 col-lg-4" >
                <div className="panel panel-info">
                    <PanelHeading title={this.props.nombre} />
                    <div className="panel-body">
                        <div className="row">
                            <div className="col-xs-6 col-sm-6 text-center">
                                <LogoImg style={{width: '80px', height:'80px'}} url={'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo } class={'img-thumbnail'} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

EquiposList.propTypes = {
    nombre: React.PropTypes.string.isRequired,
    logo: React.PropTypes.string.isRequired,
    nombre_liga: React.PropTypes.string
};
