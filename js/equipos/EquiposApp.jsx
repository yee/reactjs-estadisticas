import React from 'react';
import EquiposList from '../equipos/EquiposList';

export default class EquiposApp extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/equipos/liga/' + this.props.liga + '/torneo/' + this.props.id_torneo,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render(){
        const list = this.state.data.map((equipos, index) => {
            return <EquiposList key={ equipos.id_equipo }
                            nombre={ equipos.nombre }
                            logo={ equipos.logo }
                            nombre_liga={ equipos.nombre_liga } />
        });

        return(
            <div className="row">
                {list}
            </div>
        )
    }
}

EquiposApp.propTypes = {
    liga: React.PropTypes.string.isRequired,
    id_torneo: React.PropTypes.string.isRequired
};
