import React from 'react';
import FechaJuegosEquipoRow from './FechaJuegosEquipoRow';
import JuegosJornadaEquipoRow from './JuegosJornadaEquipoRow';

class CalendarioEquipo extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/calendario_equipo/equipo/' + this.props.equipo + '/torneo/' + this.props.torneo,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render(){
        var fecha_larga = null;
        var juegos = this.state.data.map(function(ce, index) {
            if ( fecha_larga != ce.fecha_larga ) {
                fecha_larga = ce.fecha_larga;
                return <div key={ce.id_juego}>
                            <FechaJuegosEquipoRow fecha_larga={ce.fecha_larga} jornada={ce.jornada} />
                            <JuegosJornadaEquipoRow hora={ce.hora} logo_local={ce.logo_local} logo_visita={ce.logo_visita}
                                id_local={ce.id_local} id_visita={ce.id_visita} id_juego={ce.id_juego} id_torneo={ce.id_torneo}
                                goles_local={ce.goles_local} goles_visita={ce.goles_visita} ganador={ce.ganador} />
                            <hr></hr>
                        </div>
            }
            return <JuegosJornadaEquipoRow key={ce.id_juego} hora={ce.hora} logo_local={ce.logo_local} logo_visita={ce.logo_visita}
                id_local={ce.id_local} id_visita={ce.id_visita} id_juego={ce.id_juego} id_torneo={ce.id_torneo}
                goles_local={ce.goles_local} goles_visita={ce.goles_visita} ganador={ce.ganador} />
        });
        return (
            <div>
                {juegos}
            </div>
        )
    }

}

CalendarioEquipo.propTypes = {
    equipo: React.PropTypes.string.isRequired,
    torneo: React.PropTypes.string.isRequired
};

export default CalendarioEquipo;
