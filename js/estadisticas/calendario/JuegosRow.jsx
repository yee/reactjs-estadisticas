import React from 'react';
import ReactDOM from 'react-dom';
import Marcador from './Marcador';
import LogoImg from '../../components/LogoImg';
import CalendarioEquipo from './CalendarioEquipo';

class JuegosRow extends React.Component {

    calendarioEquipo(equipo, torneo) {
        document.getElementById('container-calendario-equipo-torneo-' + torneo).innerHTML = "";
        ReactDOM.render(<CalendarioEquipo equipo={equipo} torneo={torneo} />,
            document.getElementById('container-calendario-equipo-torneo-' + torneo));
        $('#modal-calendario-equipo-' + torneo).modal('show');
    }

    render() {
        return (
            <tr>
                <td>
                    <small><b>{this.props.hora}</b></small>
                </td>
                <td>
                    <div className="col-sm-12">
                        <a href="#" onClick={ this.calendarioEquipo.bind(this, this.props.id_local, this.props.id_torneo) }>
                            <LogoImg style={{width: '35px', height: '35px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo_local } class={'img-thumbnail img-responsive'} />
                        </a>
                    </div>
                </td>
                <td>
                    <Marcador tipo={'local'} goles_local={this.props.goles_local} goles_visita={this.props.goles_visita} sizeH={'h4'} />
                </td>
                <td className="text-center">vs.</td>
                <td>
                    <Marcador tipo={'visita'} goles_local={this.props.goles_local} goles_visita={this.props.goles_visita} sizeH={'h4'} />
                </td>
                <td>
                    <div className="col-sm-12">
                        <a href="#" onClick={ this.calendarioEquipo.bind(this, this.props.id_visita, this.props.id_torneo) }>
                            <LogoImg style={{width: '35px', height: '35px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo_visita } class={'img-thumbnail img-responsive pull-right'} />
                        </a>
                    </div>
                </td>
            </tr>
        )
    }

}

export default JuegosRow;
