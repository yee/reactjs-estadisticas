import React from 'react';
import CalendarioList from './CalendarioList';

class CalendarioPanel extends React.Component {

    render() {
        var jornada = null;
        var calendario = this.props.calendario;
        var dates = [];
        var initiated = false; var number_games = 0;
        var list = this.props.calendario.map((data, index) => {

            if ( jornada != data.jornada ) {
                number_games = 0;
                dates = [];
                dates.push(data);
                jornada = data.jornada;
                number_games++;
                initiated = true;
                if ( initiated == true && number_games > 0 ) {
                    return <CalendarioList key={data.id_jornada} jornada={jornada} calendario={dates} />;
                }
            }else{
                dates.push(data); number_games++;
            }
        });

        return (
            <div className="row">
                {list}
            </div>
        )
    }

}

export default CalendarioPanel;
