import React from 'react';
import PanelHeading from '../../components/PanelHeading';
import CalendarioJornadaTable from './CalendarioJornadaTable';

class CalendarioList extends React.Component {

    render() {
        return (
            <div className="col-sm-6">
                <div className="panel panel-info">
                    <PanelHeading title={this.props.jornada} />
                    <CalendarioJornadaTable calendario={this.props.calendario} />
                </div>
            </div>
        )
    }

}

export default CalendarioList;
