import React from 'react';

class FechaJuegosRow extends React.Component {

    render() {
        return (
            <tr>
                <td colSpan={'6'} className="text-center"><small><b>{this.props.fecha_larga}</b></small></td>
            </tr>
        )
    }

}

export default FechaJuegosRow;
