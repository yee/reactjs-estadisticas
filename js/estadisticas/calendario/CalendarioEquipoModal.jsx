import React from 'react';

class CalendarioEquipoModal extends React.Component {

    render() {
        return (
            <div className="modal fade" id={'modal-calendario-equipo-' + this.props.id_torneo } tabIndex="-1" role="dialog" aria-labelledby="title-label">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="title-label">CALENDARIO/RESULTADOS</h4>
                        </div>
                        <div className="modal-body" id={'container-calendario-equipo-torneo-' + this.props.id_torneo }></div>
                    </div>
                </div>
            </div>
        )
    }

}

export default CalendarioEquipoModal;
