import React from 'react';

class Marcador extends React.Component {

    render() {
        if ( this.props.ganador != 'SIN MARCADOR' ) {
            if ( this.props.tipo == 'local' ) {
                if (this.props.goles_local > this.props.goles_visita ) {
                    var clase = 'label-success';
                }else{
                    if ( this.props.goles_local < this.props.goles_visita ) {
                        var clase = 'label-danger';
                    }else{
                        var clase = 'label-info';
                    }
                }
                var goles = this.props.goles_local;
            }else{
                if (this.props.goles_local > this.props.goles_visita ) {
                    var clase = 'label-danger';
                }else{
                    if ( this.props.goles_local < this.props.goles_visita ) {
                        var clase = 'label-success';
                    }else{
                        var clase = 'label-info';
                    }
                }
                var goles = this.props.goles_visita;
            }
            var marcador = <span className={'label ' + clase }>{goles}</span>;
        }else{
            var marcador = '';
        }

        switch ( this.props.sizeH ) {
            case 'h4':  var title = <h4>{marcador}</h4>; break;
            default:    var title = <h3>{marcador}</h3>; break;
        }

        return(
            <div className="col-xs-1 col-sm-1">
                {title}
            </div>
        )
    }

}

export default Marcador;
