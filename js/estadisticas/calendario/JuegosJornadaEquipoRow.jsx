import React from 'react';
import Marcador from './Marcador';
import LogoImg from '../../components/LogoImg';

class JuegosJornadaEquipoRow extends React.Component {

    render() {
        return (
            <div className="row">
                <div className="col-xs-2 col-sm-1">
                    <h4>{this.props.hora}</h4>
                </div>
                <div className="col-xs-3 col-sm-4">
                    <LogoImg style={{width: '50px', height: '50px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo_local } class={'img-thumbnail img-responsive'} />
                </div>
                <Marcador tipo={'local'} goles_local={this.props.goles_local} goles_visita={this.props.goles_visita} ganador={this.props.ganador} />
                <div className="col-xs-1 col-sm-1">
                    <h4>VS</h4>
                </div>
                <Marcador tipo={'visita'} goles_local={this.props.goles_local} goles_visita={this.props.goles_visita} ganador={this.props.ganador} />
                <div className="col-xs-3 col-sm-4">
                    <LogoImg style={{width: '50px', height: '50px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo_visita } class={'img-thumbnail img-responsive pull-right'} />
                </div>
            </div>
        )
    }

}

export default JuegosJornadaEquipoRow;
