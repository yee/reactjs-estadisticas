import React from 'react';

export default class FechaJuegosEquipoRow extends React.Component {

    render() {
        return (
            <div className="text-center">
                <h3>{this.props.jornada} <small>{this.props.fecha_larga}</small></h3>
            </div>
        )
    }

}
