import React from 'react';
import JuegosRow from './JuegosRow';
import FechaJuegosRow from './FechaJuegosRow';

class CalendarioJornadaTable extends React.Component {

    render() {
        return (
            <table className="table table-hover table-condensed" >
                <CalendarioRows calendario={this.props.calendario} />
            </table>
        )
    }

}

class CalendarioRows extends React.Component {

    render() {
        var fecha_larga = null;
        var juegos = this.props.calendario.map((data, index) => {
            if ( fecha_larga != data.fecha_larga ) {
                fecha_larga = data.fecha_larga;
                return <div key={data.id_juego}>
                            <FechaJuegosRow fecha_larga={data.fecha_larga} />
                            <JuegosRow hora={data.hora} logo_local={data.logo_local} logo_visita={data.logo_visita}
                                id_local={data.id_local} id_visita={data.id_visita} id_juego={data.id_juego} id_torneo={data.id_torneo}
                                goles_local={data.goles_local} goles_visita={data.goles_visita} />
                        </div>
            }
            return <JuegosRow key={data.id_juego} hora={data.hora} logo_local={data.logo_local} logo_visita={data.logo_visita}
                id_local={data.id_local} id_visita={data.id_visita} id_juego={data.id_juego} id_torneo={data.id_torneo}
                goles_local={data.goles_local} goles_visita={data.goles_visita} />
        });

        return(
            <tbody>
                {juegos}
            </tbody>
        )
    }

}

export default CalendarioJornadaTable;
