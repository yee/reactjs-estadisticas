import React from 'react';
import CalendarioPanel from './CalendarioPanel';
import CalendarioEquipoModal from './CalendarioEquipoModal';

class Calendario extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/calendario/liga/' + this.props.liga + '/torneo/' + this.props.id_torneo,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render() {
        return(
            <div id="container-calendario">
                <CalendarioEquipoModal id_torneo={this.props.id_torneo} />
                <CalendarioPanel calendario = { this.state.data } />
            </div>
        )
    }

}

Calendario.propTypes = {
    liga: React.PropTypes.string.isRequired,
    id_torneo: React.PropTypes.string.isRequired
};

export default Calendario;
