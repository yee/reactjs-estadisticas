import React from'react';
import TablaGralLocalVisitaRows from'./TablaGralLocalVisitaRows';

export default class TablaGralLocalVisitaList extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/' + this.props.type + '/liga/' + this.props.liga + '/torneo/' + this.props.id_torneo,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render() {
        let position = 1;
        let list = this.state.data.map((tblgral, index) => {
            return <TablaGralLocalVisitaRows key={ tblgral.id_equipo } position={ position++ }
                    id_torneo={ tblgral.id_torneo } id_equipo={ tblgral.id_equipo }
                    equipo={ tblgral.equipo } logo={ tblgral.logo }
                    jugados={ tblgral.jugados } ganados={ tblgral.ganados }
                    empatados={ tblgral.empatados } perdidos={ tblgral.perdidos }
                    goles_favor={ tblgral.goles_favor } goles_contra={ tblgral.goles_contra }
                    diferencia={ tblgral.diferencia } puntos={ tblgral.puntos } />
        });
        return(
            <tbody>
                {list}
            </tbody>
        )
    }

}

TablaGralLocalVisitaList.propTypes = {
    type: React.PropTypes.string.isRequired,
    liga: React.PropTypes.string.isRequired,
    id_torneo: React.PropTypes.string.isRequired
};
