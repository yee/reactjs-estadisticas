import React from 'react';
import LogoImg from '../../components/LogoImg';

export default class TablaGralLocalVisitaRows extends React.Component {

    render() {
        let efectividad;

        if ( this.props.jugados > 0 ) {
            efectividad = ((this.props.puntos * 100) / (this.props.jugados * 3)).toFixed(2);
        }else{
            efectividad = 0;
        }

        return(
            <tr>
                <td>{this.props.position}</td>
                <td>
                    <LogoImg style={{width: '35px', height: '35px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo } class={'img-thumbnail'} />
                </td>
                <td className="text-right">{this.props.jugados}</td>
                <td className="text-right">{this.props.ganados}</td>
                <td className="text-right">{this.props.empatados}</td>
                <td className="text-right">{this.props.perdidos}</td>
                <td className="text-right">{this.props.goles_favor}</td>
                <td className="text-right">{this.props.goles_contra}</td>
                <td className="text-right">{this.props.diferencia}</td>
                <td className="text-right">{this.props.puntos}</td>
                <td className="text-right">{efectividad} %</td>
            </tr>
        )
    }
}

TablaGralLocalVisitaRows.propTypes = {
    id_torneo: React.PropTypes.string.isRequired,
    id_equipo: React.PropTypes.string.isRequired,
    equipo: React.PropTypes.string.isRequired,
    logo: React.PropTypes.string.isRequired,
    position: React.PropTypes.number.isRequired,
    jugados: React.PropTypes.string.isRequired,
    ganados: React.PropTypes.string.isRequired,
    empatados: React.PropTypes.string.isRequired,
    perdidos: React.PropTypes.string.isRequired,
    goles_favor: React.PropTypes.string.isRequired,
    goles_contra: React.PropTypes.string.isRequired,
    diferencia: React.PropTypes.string.isRequired,
    puntos: React.PropTypes.string.isRequired
};
