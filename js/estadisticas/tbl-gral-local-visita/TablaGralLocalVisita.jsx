import React from 'react';
import TablaGralLocalVisitaList from './TablaGralLocalVisitaList';

export default class TablaGralLocalVisita  extends React.Component {

    render() {
        switch ( this.props.type ) {
            case 'gral': var type = "GENERAL"; break;
            case 'local': var type = "LOCALES"; break;
            case 'visita': var type = "VISITAS"; break;
        }

        return(
            <div>
                <h3>{type}</h3>
                <table className="table table-hover table-condensed" >
                    <thead>
                        <tr>
                        	<th>POSICIÓN</th>
                        	<th>EQUIPO</th>
                        	<th className="text-right">JJ</th>
                        	<th className="text-right">JG</th>
                        	<th className="text-right">JE</th>
                        	<th className="text-right">JP</th>
                        	<th className="text-right">GF</th>
                        	<th className="text-right">GC</th>
                        	<th className="text-right">DIF</th>
                        	<th className="text-right">PTS</th>
                        	<th className="text-right">% EFECTIVIDAD</th>
                        </tr>
                    </thead>
                    <TablaGralLocalVisitaList type={this.props.type} liga={this.props.liga} id_torneo={this.props.id_torneo} />
                </table>
            </div>
        )
    }

}

TablaGralLocalVisita.propTypes = {
    type: React.PropTypes.string.isRequired,
    liga: React.PropTypes.string.isRequired,
    id_torneo: React.PropTypes.string.isRequired
};
