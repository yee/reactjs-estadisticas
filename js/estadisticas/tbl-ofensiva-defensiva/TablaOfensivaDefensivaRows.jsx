import React from 'react';
import LogoImg from '../../components/LogoImg';

export default class TablaOfensivaDefensivaRows extends React.Component {

    render() {
        return(
            <tr>
                <td>{this.props.position}</td>
                <td>
                    <LogoImg style={{width: '35px', height: '35px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo } class={'img-thumbnail'} />
                </td>
                <td className="text-right">{this.props.goles_favor}{this.props.goles_contra}</td>
            </tr>
        )
    }

}
