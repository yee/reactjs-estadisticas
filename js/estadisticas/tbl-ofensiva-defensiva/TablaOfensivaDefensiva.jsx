import React from 'react';
import TablaOfensivaDefensivaTbl from './TablaOfensivaDefensivaTbl';

export default class TablaOfensivaDefensiva extends React.Component {

    render() {
        return(
            <div className="row">
                <div className="col-sm-6">
                    <h3>OFENSIVAS</h3>
                    <TablaOfensivaDefensivaTbl liga={this.props.liga} id_torneo={this.props.id_torneo} title={'GOLES ANOTADOS'} abrev={'G.A.'} />
                </div>
                <div className="col-sm-6">
                    <h3>DEFENSIVAS</h3>
                    <TablaOfensivaDefensivaTbl liga={this.props.liga} id_torneo={this.props.id_torneo} title={'GOLES RECIBIDOS'} abrev={'G.R.'}  />
                </div>
            </div>
        )
    }

}
