import React from 'react';
import TablaOfensivaDefensivaRows from './TablaOfensivaDefensivaRows';

export default class TablaOfensivaDefensivaTbody extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: this.props.url,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render() {
        var position = 1;
        var type = this.props.type;
        var list = this.state.data.map((tblod, index) => {
            if ( type == 'ofensiva' ) {
                return <TablaOfensivaDefensivaRows key={ tblod.id_equipo } position={ position++ }
                        id_torneo={ tblod.id_torneo } id_equipo={ tblod.id_equipo }
                        equipo={ tblod.equipo } logo={ tblod.logo }
                        goles_favor={ tblod.goles_favor } />
            }else{
                return <TablaOfensivaDefensivaRows key={ tblod.id_equipo } position={ position++ }
                        id_torneo={ tblod.id_torneo } id_equipo={ tblod.id_equipo }
                        equipo={ tblod.equipo } logo={ tblod.logo }
                        goles_favor={ tblod.goles_contra } />
            }
        });
        return(
            <tbody>
                {list}
            </tbody>
        )
    }
}

TablaOfensivaDefensivaTbody.propTypes = {
    url: React.PropTypes.string.isRequired
};
