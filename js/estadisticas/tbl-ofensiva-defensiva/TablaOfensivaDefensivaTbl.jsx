import React from 'react';
import TablaOfensivaDefensivaTbody from './TablaOfensivaDefensivaTbody';

export default class TablaOfensivaDefensivaTbl extends React.Component {

    render() {

        if (this.props.title=='GOLES ANOTADOS') {
            var type = 'ofensiva';
            var url = 'http://antonioyee.mx/quiniela-app/api/service/ofensiva/liga/'+ this.props.liga +'/torneo/'+ this.props.id_torneo;
        }else{
            var type = 'defensiva';
            var url = 'http://antonioyee.mx/quiniela-app/api/service/defensiva/liga/'+ this.props.liga +'/torneo/'+ this.props.id_torneo;
        }

        return(
            <table className="table table-hover table-condensed">
                <thead>
                    <tr>
						<th><span className="hidden-xs">POSICIÓN</span><span className="visible-xs">#</span></th>
						<th><span className="hidden-xs">EQUIPO</span></th>
						<th className="text-right"><span className="hidden-xs">{this.props.title}</span><span className="visible-xs">{this.props.abrev}</span></th>
					</tr>
                </thead>
                <TablaOfensivaDefensivaTbody liga={this.props.liga} id_torneo={this.props.id_torneo} url={url} type={type} />
            </table>
        )
    }

}
