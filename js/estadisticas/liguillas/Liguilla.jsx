import React from 'react';
import LiguillaTBody from './LiguillaTBody';

class Liguilla extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/liguilla/torneo/' + this.props.id_torneo,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render() {
        return(
            <table className="table table-hover table-condensed" >
                <LiguillaTBody liguilla={this.state.data} liga={this.props.liga} torneo={this.props.id_torneo} />
			</table>
        )
    }
}

Liguilla.propTypes = {
    id_torneo: React.PropTypes.string.isRequired,
    liga: React.PropTypes.string.isRequired
};

export default Liguilla;
