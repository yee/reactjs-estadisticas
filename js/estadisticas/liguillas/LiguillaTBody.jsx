import React from 'react';
import LiguillaRow from './LiguillaRow';
import LiguillaLiderAscenso from './LiguillaLiderAscenso';

class LiguillaTBody extends React.Component {

    render() {
        let liga = this.props.liga;
        let torneo = this.props.torneo;
        let liguilla = this.props.liguilla;

        var rows = this.props.liguilla.map((data, index) => {
            switch (index) {
                case 0:
                    // solo ascenso mx y cl2015 para atras habia menos equipos (liguilla solo 7)
                    // 1 lider gral del torneo, accedia a semifinales directo
                    if ( liga == 4 && torneo <= 21 ) {
                        return <LiguillaLiderAscenso key={ data.id_equipo } logo={ data.logo } posicion={ index + 1 } puntos={ data.puntos } />
                    }else{
                        return <LiguillaRow key={ data.id_equipo } logo={ data.logo } posicion={ index + 1 } puntos={ data.puntos }
                                logo_rival={liguilla[liguilla.length - 1].logo} posicion_rival={ liguilla.length } puntos_rival={ liguilla[liguilla.length - 1].puntos } />
                    }
                break;
                case 1:
                    return <LiguillaRow key={ data.id_equipo } logo={ data.logo } posicion={ index + 1 } puntos={ data.puntos }
                            logo_rival={liguilla[liguilla.length - 2].logo} posicion_rival={ liguilla.length - 1 } puntos_rival={ liguilla[liguilla.length - 1].puntos } />
                break;
                case 2:
                    return <LiguillaRow key={ data.id_equipo } logo={ data.logo } posicion={ index + 1 } puntos={ data.puntos }
                            logo_rival={liguilla[liguilla.length - 3].logo} posicion_rival={ liguilla.length - 2 } puntos_rival={ liguilla[liguilla.length - 1].puntos } />
                break;
                case 3:
                    return <LiguillaRow key={ data.id_equipo } logo={ data.logo } posicion={ index + 1 } puntos={ data.puntos }
                            logo_rival={liguilla[liguilla.length - 4].logo} posicion_rival={ liguilla.length - 3 } puntos_rival={ liguilla[liguilla.length - 1].puntos } />
                break;
            }
        });
        return(
            <tbody>
                {rows}
            </tbody>
        )
    }

}

LiguillaTBody.propTypes = {
    liguilla: React.PropTypes.array.isRequired,
    liga: React.PropTypes.string.isRequired,
    torneo: React.PropTypes.string.isRequired
};

export default LiguillaTBody;
