import React from 'react';
import LiguillaEquipo from './LiguillaEquipo';

class LiguillaRow extends React.Component {

    render() {
        return(
            <tr>
				<td>
					<div className="row">
                        <LiguillaEquipo posicion={this.props.posicion} logo={this.props.logo} puntos={this.props.puntos} />
					</div>
				</td>
				<td className="text-center"><strong>vs</strong></td>
				<td>
					<div className="row pull-right">
                        <LiguillaEquipo posicion={this.props.posicion_rival} logo={this.props.logo_rival} puntos={this.props.puntos_rival} />
					</div>
				</td>
			</tr>
        )
    }

}

LiguillaRow.propTypes = {
    logo: React.PropTypes.string.isRequired,
    posicion: React.PropTypes.number.isRequired,
    puntos: React.PropTypes.string.isRequired,
    logo_rival: React.PropTypes.string.isRequired,
    posicion_rival: React.PropTypes.number.isRequired,
    puntos_rival: React.PropTypes.string.isRequired
};

export default LiguillaRow;
