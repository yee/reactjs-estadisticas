import React from 'react';
import LogoImg from '../../components/LogoImg';

class LiguillaEquipo extends React.Component {

    render() {
        return(
            <div className="col-sm-12">
                <strong># </strong>
                <span className="label label-success">{this.props.posicion}</span>
                <span> </span>
                <LogoImg style={{width: '50px', height: '50px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo } class={'img-thumbnail'} />
                <span> </span>
                <span className="label label-info">{this.props.puntos} puntos</span>
            </div>
        )
    }

}

LiguillaEquipo.propTypes = {
    logo: React.PropTypes.string.isRequired,
    posicion: React.PropTypes.number.isRequired,
    puntos: React.PropTypes.string.isRequired
};

export default LiguillaEquipo;
