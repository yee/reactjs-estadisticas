import React from 'react';
import LogoImg from '../../components/LogoImg';

class LiguillaLiderAscenso extends React.Component {

    render() {
        return(
            <tr>
				<td colSpan="3" className="text-center">
					<div className="row">
						<div className="col-sm-12">
							<strong># </strong>
                            <span className="label label-success">{this.props.posicion}</span>
                            <span> </span>
                            <LogoImg style={{width: '50px', height: '50px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo } class={'img-thumbnail'} />
                            <span> </span>
                            <span className="label label-info">{this.props.puntos} puntos</span>
                            <span> <strong>Semifinales directo</strong></span>
                        </div>
					</div>
				</td>
			</tr>
        )
    }

}

LiguillaLiderAscenso.propTypes = {
    logo: React.PropTypes.string.isRequired,
    posicion: React.PropTypes.number.isRequired,
    puntos: React.PropTypes.string.isRequired
};

export default LiguillaLiderAscenso;
