import React from 'react';
import GoleoBody from './GoleoBody';
import GoleoFoot from './GoleoFoot';

class Goleo extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/' + this.props.type + '/liga/' + this.props.liga + '/torneo/' + this.props.id_torneo,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render() {
        var table = '';
        if ( this.state.data.length > 0 ) {
            table = <table className="table table-hover table-condensed" >
                		<thead>
                			<tr>
                				<th width={'20%'}>JORNADA</th>
                				<th className="text-center">
                					<span className="visible-xs visible-sm">GL</span>
                					<span className="hidden-xs hidden-sm">GOLES LOCAL</span>
                				</th>
                				<th className="text-center">
                					<span className="visible-xs visible-sm">PL</span>
                					<span className="hidden-xs hidden-sm">PROMEDIO LOCAL</span>
                				</th>
                				<th className="text-center">
                					<span className="visible-xs visible-sm">GV</span>
                					<span className="hidden-xs hidden-sm">GOLES VISITA</span>
                				</th>
                				<th className="text-center">
                					<span className="visible-xs visible-sm">PV</span>
                					<span className="hidden-xs hidden-sm">PROMEDIO VISITA</span>
                				</th>
                				<th className="text-center">
                					<span className="visible-xs visible-sm">GJ</span>
                					<span className="hidden-xs hidden-sm">GOLES JORNADA</span>
                				</th>
                				<th className="text-center">
                					<span className="visible-xs visible-sm">PJ</span>
                					<span className="hidden-xs hidden-sm">PROMEDIO JORNADA</span>
                				</th>
                			</tr>
                		</thead>
                        <GoleoBody goleo={this.state.data} />
                        <GoleoFoot goleo={this.state.data} type={this.props.type} />
                	</table>
        }else{
            if ( this.props.type == 'goles_regular' ) {
                var msg = 'Aún no comienza el torneo'
            }else{
                var msg = 'Aún no comienza la liguilla';
            }
            table = <div className="alert alert-success text-center">
                        <strong>{msg}</strong>
                    </div>
        }
        return(
            <div>{table}</div>
        )
    }
}

Goleo.propTypes = {
    type: React.PropTypes.string.isRequired,
    liga: React.PropTypes.string.isRequired,
    id_torneo: React.PropTypes.string.isRequired
};

export default Goleo;
