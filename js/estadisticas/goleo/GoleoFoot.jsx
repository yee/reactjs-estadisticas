import React from 'react';
import GoleoRow from './GoleoRow';

class GoleoFoot extends React.Component {

    render() {
        if ( this.props.type == 'goles_regular' ) {
            var title = 'TOTAL DEL TORNEO';
        }else{
            var title = 'TOTAL EN LIGUILLA';
        }

        let locales = 0, visitas = 0, total = 0;

        this.props.goleo.map((gol, index) => {
            locales += parseInt(gol.gol_locales);
            visitas += parseInt(gol.gol_visitas);
            total += parseInt(gol.juegos_totales);
        });

        return(
            <tfoot>
                <GoleoRow jornada={title} gol_locales={locales} gol_visitas={visitas} juegos_totales={total}  />
            </tfoot>
        )
    }
}

GoleoFoot.propTypes = {
    type: React.PropTypes.string.isRequired,
    goleo: React.PropTypes.array.isRequired
};

export default GoleoFoot;
