import React from 'react';
import GoleoRow from './GoleoRow';

class GoleoBody extends React.Component {

    render() {
        let rows = this.props.goleo.map((gol, index) => {
            return <GoleoRow key={ gol.JornadaID }
                    jornada={ gol.jornada } gol_locales={ gol.gol_locales }
                    gol_visitas={ gol.gol_visitas } juegos_totales={ gol.juegos_totales } />
        });
        return(
            <tbody>
                {rows}
            </tbody>
        )
    }
}

GoleoBody.propTypes = {
    goleo: React.PropTypes.array.isRequired
};

export default GoleoBody;
