import React from 'react';

class GoleoRow extends React.Component {

    render() {
        let goles_jornada = parseInt(this.props.gol_locales) + parseInt(this.props.gol_visitas);
        let promedio_local = (parseInt(this.props.gol_locales) / parseInt(this.props.juegos_totales)).toFixed(2);
        let promedio_visita = (parseInt(this.props.gol_visitas) / parseInt(this.props.juegos_totales)).toFixed(2);
        let promedio_jornada = (goles_jornada / parseInt(this.props.juegos_totales)).toFixed(2);
        return(
            <tr>
                <td><strong>{this.props.jornada}</strong></td>
                <td className="text-center">{this.props.gol_locales}</td>
                <td className="text-center">{promedio_local}</td>
                <td className="text-center">{this.props.gol_visitas}</td>
                <td className="text-center">{promedio_visita}</td>
                <td className="text-center">{goles_jornada}</td>
                <td className="text-center">{promedio_jornada}</td>
            </tr>
        )
    }

}

export default GoleoRow;
