import React from 'react';
import ReactDOM from 'react-dom';
import EquiposApp from '../equipos/EquiposApp';
import Goleo from '../estadisticas/goleo/Goleo';
import Liguilla from '../estadisticas/liguillas/Liguilla';
import Calendario from '../estadisticas/calendario/Calendario';
import TablaGralLocalVisita from '../estadisticas/tbl-gral-local-visita/TablaGralLocalVisita';
import TablaCociente from '../estadisticas/tbl-cociente/TablaCociente';
import TablaOfensivaDefensiva from '../estadisticas/tbl-ofensiva-defensiva/TablaOfensivaDefensiva';

export default class EstadisticasApp extends React.Component {

    clickFunctions() {
        document.getElementById('container-estadisticas-' + this.props.id_torneo).innerHTML = "";
        return { liga : this.props.liga, id_torneo : this.props.id_torneo, id_temporada : this.props.id_temporada };
    }

    equipos() {
        ReactDOM.render(<EquiposApp liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    calendario() {
        ReactDOM.render(<Calendario liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    tblGral() {
        ReactDOM.render(<TablaGralLocalVisita type={'tabla_general'}  liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    tblLocal() {
        ReactDOM.render(<TablaGralLocalVisita type={'local'} liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    tblVisita() {
        ReactDOM.render(<TablaGralLocalVisita type={'visita'} liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    tblOfensivaDefensiva() {
        ReactDOM.render(<TablaOfensivaDefensiva liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }
    tblCociente() {
        ReactDOM.render( <TablaCociente id_temporada={this.clickFunctions().id_temporada} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    goleoJornadas() {
        ReactDOM.render( <Goleo type={'goles_regular'} liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    goleoLiguilla() {
        ReactDOM.render( <Goleo type={'goles_liguilla'} liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    liguilla() {
        ReactDOM.render(<Liguilla liga={this.clickFunctions().liga} id_torneo={this.clickFunctions().id_torneo} />,
                        document.getElementById('container-estadisticas-' + this.clickFunctions().id_torneo));
    }

    render() {
        const btnStyle = { 'borderRadius': '0px', 'textAlign': 'left' };
        let cociente, liguilla, goleo_liguilla = '';

        if ( this.props.liga == 1 ) { // liga mx
            cociente = <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.tblCociente.bind(this)}>Cociente</a>
        }

        if ( this.props.liga == 1 || this.props.liga == 4 ) {
            liguilla = <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.liguilla.bind(this)}>Liguilla Hoy</a>
            goleo_liguilla = <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.goleoLiguilla.bind(this)}>Goleo en Liguilla</a>
        }

        return (
            <div className="panel panel-primary">
                <div className="panel-heading">
                    <h3 className="panel-title">Estadísticas del Torneo {this.props.torneo}</h3>
                </div>
                <div className="panel-body">
                    <div className="row">
                        <div className="col-sm-3 estadisticas">
                            <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.equipos.bind(this)}>Equipos</a>
                            <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.calendario.bind(this)}>Calendario</a>
                            <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.tblGral.bind(this)}>Tabla General</a>
                            <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.tblLocal.bind(this)}>Tabla Local</a>
                            <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.tblVisita.bind(this)}>Tabla Visita</a>
                            <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.tblOfensivaDefensiva.bind(this)}>Ofensivas / Defensivas</a>
                            {cociente}
                            <a className="btn btn-menu list-group-item" style={btnStyle} onClick={this.goleoJornadas.bind(this)}>Goleo por Jornada</a>
                            {liguilla}
                            {goleo_liguilla}
                        </div>
                        <div className="col-sm-9" id={ 'container-estadisticas-' + this.props.id_torneo }></div>
                    </div>
                </div>
            </div>
        )
    }

}

EstadisticasApp.propTypes = {
    id_temporada: React.PropTypes.string,
    torneo: React.PropTypes.string.isRequired,
    id_torneo: React.PropTypes.string.isRequired,
    liga: React.PropTypes.string.isRequired,
};
