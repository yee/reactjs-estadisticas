import React from 'react';
import TablaCocientelList from './TablaCocienteList';

class TablaCociente extends React.Component {

    render() {
        return(
            <div>
                <h3>COCIENTE</h3>
                <table className="table table-hover table-condensed" >
                    <thead>
                        <tr>
                            <th><span className="hidden-xs">POSICIÓN</span><span className="visible-xs">#</span></th>
                            <th>EQUIPO</th>
                            <th className="text-right"><span className="hidden-xs">TOTAL JUEGOS JUGADOS</span><span className="visible-xs">TJJ</span></th>
                            <th className="text-right"><span className="hidden-xs">TOTAL PUNTOS</span><span className="visible-xs">TPTS</span></th>
                            <th className="text-right">COCIENTE</th>
                        </tr>
                    </thead>
                    <TablaCocientelList id_temporada={this.props.id_temporada} />
                </table>
            </div>
        )
    }

}

TablaCociente.propTypes = {
    id_temporada: React.PropTypes.string.isRequired
};

export default TablaCociente;
