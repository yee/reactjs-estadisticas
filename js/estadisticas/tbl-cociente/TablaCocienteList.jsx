import React from 'react';
import TablaCocienteRows from './TablaCocienteRows';

class TablaCocientelList extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/cociente/temporada/' + this.props.id_temporada,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render() {
        let position = 1;
        let list = this.state.data.map((cocientes, index) => {
            return <TablaCocienteRows key={ cocientes.equipo_id } position={ position++ }
                    equipo_id={ cocientes.equipo_id } equipo={ cocientes.equipo }
                    logo={ cocientes.logo } jj={ cocientes.jj }
                    puntos={ cocientes.puntos } />
        });
        return(
            <tbody>
                {list}
            </tbody>
        )
    }

}

TablaCocientelList.propTypes = {
    id_temporada: React.PropTypes.string.isRequired
};

export default TablaCocientelList;
