import React from 'react';
import LogoImg from '../../components/LogoImg';

class TablaCocienteRows extends React.Component {

    render() {
        let cociente, classRow, positionComponent;

        if ( this.props.jj > 0 ) {
			cociente = (this.props.puntos / this.props.jj ).toFixed(4);
		}else{
			cociente = 0;
		}

        if ( this.props.position == 18 ) {
			classRow = 'danger';
		}else{
			if ( this.props.position < 18 && this.props.position >= 15 ) {
				classRow = 'warning';
			}else{
				classRow = '';
			}
		}

        if ( this.props.position <= 18 && this.props.position >= 15 ) {
            positionComponent = <label className="label label-danger">{this.props.position}</label>
        }else{
            positionComponent = <span>{this.props.position}</span>
        }

        return(
            <tr className={classRow}>
				<td>{positionComponent}</td>
                <td>
                    <LogoImg style={{width: '35px', height: '35px'}} url={ 'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo } class={'img-thumbnail'} />
                </td>
				<td className="text-right">{this.props.jj}</td>
				<td className="text-right">{this.props.puntos}</td>
				<td className="text-right">
                    {cociente}
				</td>
			</tr>
        )
    }

}

TablaCocienteRows.propTypes = {
    position: React.PropTypes.number.isRequired,
    equipo_id: React.PropTypes.string.isRequired,
    equipo: React.PropTypes.string.isRequired,
    logo: React.PropTypes.string.isRequired,
    jj: React.PropTypes.string.isRequired,
    puntos: React.PropTypes.string.isRequired
};

export default TablaCocienteRows;
