import React    from 'react';
import LigaRow  from '../ligas/LigaRow';

export default class LigaList extends React.Component {

    render (){
        const list = this.props.catalogo.map((ligas, index) => {
            return <LigaRow key={ ligas.id_liga }
                            liga={ ligas.id_liga }
                            nombre={ ligas.nombre }
                            logo={ ligas.logo }
                            menu={ ligas.menu } />
        });

        return (
            <div className="container-fluid">
                <ul className="media-list">
                    {list}
                </ul>
            </div>
        )
    }

}

LigaList.propTypes = { catalogo: React.PropTypes.array.isRequired };
