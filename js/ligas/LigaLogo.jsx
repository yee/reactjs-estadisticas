import React    from 'react';
import LogoImg  from '../components/LogoImg';

export default class LigaLogo extends React.Component {

    render(){
        return (
            <figure className="media-left">
                <LogoImg style={{width: '64px'}} url={'http://antonioyee.mx/quiniela-app/logos/mini/' + this.props.logo } class={'media-object'} />
            </figure>
        )
    }

}

LigaLogo.propTypes = {
    logo: React.PropTypes.string.isRequired
};
