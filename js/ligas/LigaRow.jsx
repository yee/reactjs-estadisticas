import React        from 'react';
import ReactDOM     from 'react-dom';
import LigaLogo     from '../ligas/LigaLogo';
import TorneoApp    from '../torneos/TorneoApp';

export default class LigaRow extends React.Component {

    constructor(props){
        super(props);
    }

    handleClick (){
        const liga = this.props.liga;
        ReactDOM.render(<TorneoApp liga={liga} />, document.getElementById('contenedor-torneo-' + liga));
    }

    render (){
        const style = { 'textDecoration': 'none'};
        const href  = '#' + this.props.menu;

        return(
            <li className="media">
                <LigaLogo logo={this.props.logo} />
                <div className="media-body" onClick={ this.handleClick.bind(this) } >
                    <a href={'#' + this.props.menu} style={style}><h4>{this.props.nombre}</h4></a>
                    <div id={'contenedor-torneo-' + this.props.liga }></div>
                </div>
                <hr/>
            </li>
        )
    }

}

LigaRow.propTypes = {
    menu: React.PropTypes.string.isRequired,
    logo: React.PropTypes.string.isRequired,
    nombre: React.PropTypes.string.isRequired,
    liga: React.PropTypes.string.isRequired
};
