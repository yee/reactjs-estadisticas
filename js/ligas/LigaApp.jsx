import React    from 'react';
import LigaList from '../ligas/LigaList';

export default class LigaApp extends React.Component {

    constructor(props){
        super(props);
        this.state = { data: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/ligas',
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render(){
        if ( this.state.data.length > 0 ) {
            return (
                <div className="container-fluid">
                    <LigaList catalogo={ this.state.data } />
                </div>
            )
        }else{
            return (
                <div className="text-center">
                    <span className="label label-info">Cargando Ligas...</span>
                </div>
            )
        }
    }

}
