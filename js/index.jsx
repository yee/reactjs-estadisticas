import React from 'react';
import ReactDOM from 'react-dom';

import LigaApp from './ligas/LigaApp';

ReactDOM.render(<LigaApp />, document.getElementById('contenedor'));
