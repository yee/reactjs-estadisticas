import React from 'react';

export default class PanelHeading extends React.Component {

    render() {
        return (
            <div className="panel-heading">
                <h3 className="panel-title">
                    <span>{this.props.title}</span>
                </h3>
            </div>
        )
    }

}
