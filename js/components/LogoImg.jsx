import React from 'react';

export default class LogoImg extends React.Component {

    render(){
        return (
            <img style={this.props.style} src={this.props.url} className={this.props.class} />
        )
    }

}

LogoImg.propTypes = {
    style: React.PropTypes.object,
    url: React.PropTypes.string.isRequired,
    class: React.PropTypes.string
};
