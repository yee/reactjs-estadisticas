import React        from 'react';
import TorneoList   from '../torneos/TorneoList';

export default class TorneoApp extends React.Component {

    constructor(props){
        super(props);
        this.state = { torneos: [] };
    }

    componentWillMount() {
        $.ajax({
			url: 'http://antonioyee.mx/quiniela-app/api/service/torneos/id/' + this.props.liga,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({torneos: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.url, status, err.toString());
			}.bind(this)
		});
    }

    render(){
        if ( this.state.torneos.length > 0 ) {
            return (
                <div className="container-fluid">
                    <TorneoList catalogotorneos={ this.state.torneos } liga={this.props.liga} />
                </div>
            )
        }else{
            return (
                <div className="text-center">
                    <span className="label label-info">Cargando Torneos...</span>
                </div>
            )
        }
    }
}

TorneoApp.propTypes = {
    liga: React.PropTypes.string.isRequired
};
