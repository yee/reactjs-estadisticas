import React            from 'react';
import ReactDOM         from 'react-dom';
import TorneoName       from '../torneos/TorneoName';
import EstadisticasApp  from '../estadisticas/EstadisticasApp';

export default class TorneoRow extends React.Component {

    handleClick() {
        const id_torneo     = this.props.id_torneo;
        const torneo        = this.props.torneo;
        const id_temporada  = this.props.id_temporada;
        document.getElementById('container-estadisticas-liga-' + this.props.liga ).innerHTML = "";
        ReactDOM.render( <EstadisticasApp liga={this.props.liga} id_torneo={id_torneo} torneo={torneo} id_temporada={id_temporada} />, document.getElementById('container-estadisticas-liga-' + this.props.liga ));
    }

    render() {
        return(
            <button type="button" className="btn btn-sm btn-success btn-torneo" onClick={this.handleClick.bind(this)} >
                <TorneoName torneo={this.props.torneo} />
            </button>
        )
    }

}

TorneoRow.propTypes = {
    id_temporada: React.PropTypes.string,
    torneo: React.PropTypes.string.isRequired,
    id_torneo: React.PropTypes.string.isRequired,
    liga: React.PropTypes.string.isRequired,
};
