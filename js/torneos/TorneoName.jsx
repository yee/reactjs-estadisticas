import React from 'react';

export default class TorneoName extends React.Component{

    render () {
        return <span>{this.props.torneo}</span>
    }

}

TorneoName.propTypes = {
    torneo: React.PropTypes.string.isRequired
};
