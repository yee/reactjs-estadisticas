import React        from 'react';
import TorneoRow    from '../torneos/TorneoRow';

export default class TorneoList extends React.Component {

    render () {
        const list = this.props.catalogotorneos.map((torneos, index) => {
            return <TorneoRow key={ torneos.id_torneo } id_torneo={ torneos.id_torneo } torneo={ torneos.torneo }
                                liga={ torneos.id_liga } id_temporada={ torneos.id_temporada } />
        });
        return (
            <div className="container-fluid">
                <div className="btn-group" role="group">
                    {list}
                </div>
                <br/>
                <br/>
                <div id={'container-estadisticas-liga-' + this.props.liga }></div>
            </div>
        )
    }
}

TorneoList.propTypes = {
    catalogotorneos: React.PropTypes.array.isRequired
};
