var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('default', ['js', 'css', 'fonts']);

gulp.task('js', function () {
    gulp.src([
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/bootstrap/dist/js/bootstrap.min.js'])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/'))
});

gulp.task('css', function () {
    gulp.src('./node_modules/bootstrap/dist/css/bootstrap.min.css')
    .pipe(gulp.dest('./css/'));
});

gulp.task('fonts', function() {
    gulp.src('./node_modules/bootstrap/dist/fonts/**/*')
    .pipe(gulp.dest('./fonts/'));
});
